let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/

mix.js('resources/assets/js/jquery-3.3.1.js','public/js/jquery-3.3.1.min.js');

mix.styles('resources/assets/bootstrap-4.0.0/dist/css/bootstrap.css','public/css/bootstrap.min.css');
mix.js('resources/assets/bootstrap-4.0.0/dist/js/bootstrap.js','public/js/bootstrap.min.js');
