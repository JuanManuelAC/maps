<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA02pxdfQK4FqzYKKQ1AVwMdeRusi2vEAA&callback=initMap"></script>
<script>
    function initMap() {

        /**
         * @Ubication Pereira
        */
        var uluru = {lat: 4.8133300, lng: -75.6961100};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });

        /**
         * @Ubication Armenia
         */
        var armeniauluru = {lat: 4.5338900, lng: -75.6811100};
        var armeniamap = new google.maps.Map(document.getElementById('armenia-map'), {
            zoom: 13,
            center: armeniauluru
        });
        var markerArmenia = new google.maps.Marker({
            position: armeniauluru,
            map: armeniamap
        });
    }
</script>